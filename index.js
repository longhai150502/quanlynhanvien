var dsNhanVien = [];

function capNhatLocalStorage(){
    let jsonDsNhanVien = JSON.stringify(dsNhanVien);
    localStorage.setItem("danhSachNhanVien", jsonDsNhanVien)
}

var dataJson = localStorage.getItem("danhSachNhanVien")
if(dataJson!== null){
    var nhanVienArr = JSON.parse(dataJson);
    for(var index = 0; index < nhanVienArr.length; index++){
        var item = nhanVienArr[index];
        var nhanVien = new NhanVien(item.taiKhoan, item.hoTen, item.email, item.matKhau, item.ngayLam, item.luongCoBan, item.chucVu, item.gioLam, item.loaiNhanVien, item.tongLuong);
        dsNhanVien.push(nhanVien);
        
    }
    renderDsNhanVien(dsNhanVien)
}

function themNhanVien(){
    var nhanVien = layThongTinTuForm();
    var isValid = true;
    isValid = kiemTraTrung(nhanVien.taiKhoan, dsNhanVien)
    if (isValid){
        dsNhanVien.push(nhanVien);
        capNhatLocalStorage();
        renderDsNhanVien(dsNhanVien);
    }
}

function xoaNhanVien(taiKhoan){
    var viTri = timKiemViTri(taiKhoan, dsNhanVien);

    if(viTri !== -1){
        dsNhanVien.splice(viTri, 1);
        capNhatLocalStorage();
        renderDsNhanVien(dsNhanVien);
    }
}

function suaNhanVien(taiKhoan) {
    var viTri = timKiemViTri(taiKhoan, dsNhanVien);
    if(viTri == -1) return;
    var data = dsNhanVien[viTri];
    showThongTin(data)
    document.getElementById("tknv").disabled = true;
}

function capNhatNhanVien(){
    var data = layThongTinTuForm()
    var viTri = timKiemViTri(data.taiKhoan, dsNhanVien);
    if(viTri == -1) return;
    dsNhanVien[viTri] = data;
    renderDsNhanVien(dsNhanVien);
    capNhatLocalStorage();
    document.getElementById("tknv").disabled = false;
    
}

function kiemTraTrung(taiKhoan, dsNhanVien){
    let index = timKiemViTri(taiKhoan, dsNhanVien);
    if (index !== -1) {
        document.getElementById("tbTKNV").innerHTML = "Tài khoản đã tồn tại";
        return false ;
    }else {
        document.getElementById("tbTKNV").innerHTML = "";
        return true;
    }
}


var inputSearch = document.getElementById("searchName");
inputSearch.addEventListener("keypress", function(event) {
    if (event.key === "Enter") {
      event.preventDefault();
      document.getElementById("btnTimNV").click();
    }
});
function timKiemNhanVien(){
    var nhanVienTheoLoai = [];
    for(var i = 0; i<dsNhanVien.length; i++){
        if(inputSearch.value == dsNhanVien[i].loaiNhanVien){
            nhanVienTheoLoai.push(dsNhanVien[i]);
            console.log(' :>> ',nhanVienTheoLoai );
                 
        }
        renderDsNhanVien(nhanVienTheoLoai);
    }
}
    