


function Validator(options){
    console.log('options', options)
    function getParent(element, selector) {
        while (element.parentElement) {
            if(element.parentElement.matches(selector)){
                return element.parentElement;
            }
            element = element.parentElement;
        }
    }

    var selectorRules = {};

    function validate(inputElement, rule) {
        var errorElement = getParent(inputElement, options.formGroupSelector).querySelector(options.errorSelector);
        var errorMessage; 

        var rules = selectorRules[rule.selector];
        for(var i = 0; i< rules.length; i++){
            errorMessage = rules[i](inputElement.value);
            if(errorMessage) 
            break;
        }

                    if(errorMessage){
                        errorElement.innerHTML = errorMessage;
                        
                    } else {
                        errorElement.innerHTML = '';
                        
                    }
                return !errorMessage;
    }


    var formElement = document.querySelector(options.form);
    var buttonAddNhanVien = document.querySelector(options.buttonAdd);
    var buttonUpdateNhanVien = document.querySelector(options.buttonUpdate);

    if(formElement){
        
        options.rules.forEach(function (rule){

            if(Array.isArray(selectorRules[rule.selector])){
                selectorRules[rule.selector].push(rule.test);
            } else {
                selectorRules[rule.selector] = [rule.test];
            }

            var inputElement = formElement.querySelector(rule.selector);
            
            if(inputElement) {
                inputElement.onblur = function (){
                    validate(inputElement, rule);
                }

                inputElement.oninput = function(){
                    var errorElement = getParent(inputElement, options.formGroupSelector).querySelector(options.errorSelector);
                    errorElement.innerText = '';
                }
            }
        });

    }

    if(buttonAddNhanVien){
        buttonAddNhanVien.onclick = function(){
            
            var isFormValid = true;
            
            options.rules.forEach(function (rule){
                var inputElement = formElement.querySelector(rule.selector);
                var isValid = validate(inputElement, rule);
                if(!isValid){
                    isFormValid = false;
                }
            });
            if(isFormValid){   
                themNhanVien();
            }
        }
    }

    if(buttonUpdateNhanVien){
        buttonUpdateNhanVien.onclick = function(){

            var isFormValid = true;
            options.rules.forEach(function (rule){
                var inputElement = formElement.querySelector(rule.selector);
                var isValid = validate(inputElement, rule);
                if(!isValid){
                    isFormValid = false;
                }
            });
            if(isFormValid){
                capNhatNhanVien();
            }
        }
    }
}


// dinh nghia
Validator.isRequired = function(selector){
    console.log('selector :>> ', selector);
    return {
        selector: selector,
        test: function(value){
            return value.trim() ? undefined: "* Vui lòng nhập trường này"    
        }
    }
}

Validator.isAccount = function(selector){
    return {
        selector: selector,
        test: function(value){
            var regexAccount = /^[a-zA-Z0-9\+]*$/;
            return regexAccount.test(value) ? undefined :"* Tài khoản không được nhập kí tự đặc biệt "
        }
    }
}

Validator.maxLengthAccount = function(selector, max){
    return {
        selector: selector,
        test: function(value){
           return max>=value.length ? undefined: `* Tài khoản chỉ nhập tối đa ${max} kí tự`;
        }
    }
}

Validator.isName = function(selector){
    return {
        selector: selector,
        test: function(value){
            var regex = /(['aAàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬbBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆfFgGhHiIìÌỉỈĩĨíÍịỊjJkKlLmMnNoOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢpPqQrRsStTuUùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰvVwWxXyYỳỲỷỶỹỸýÝỵỴzZ '])/;
            return regex.test(value) ? undefined :"* Vui lòng nhập đúng định dạng"
        }
    }
}

Validator.isEmail = function(selector){
    return {
        selector: selector,
        test: function(value){
            var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            return regex.test(value) ? undefined :"* Trường này phải là Email"
        }
    }
}

Validator.passwordLength = function(selector, min){
    return {
        selector: selector,
        test: function(value){
           return (min<=value.length) ? undefined: `* Vui lòng nhập tối thiểu ${min} kí tự`;
        }
    }
}

Validator.isPassword = function(selector){
    return {
        selector: selector,
        test: function(value){
            var regexPassword = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/;
            return regexPassword.test(value) ? undefined :"* Mật khẩu phải chứa ít nhất 1 số, 1 chữ hoa, 1 chữ thường"
        }
    }
}

Validator.isGoodDate = function(selector){
    return {
        selector: selector,
        test: function(value){
            var reGoodDate = /^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/;
            return reGoodDate.test(value) ? undefined :"* Nhập định dạng MM/DD/YYYY"
        }
    }
}

Validator.maxLuong = function(selector, min, max){
    return {
        selector: selector,
        test: function(value){
            return (min<=value && value <= max) ? undefined: `* Lương cơ bản từ ${min} - ${max}`;
        }
    }
}

Validator.gioLam = function(selector, min, max){
    return {
        selector: selector,
        test: function(value){
            return (min<=value && value <= max) ? undefined: `* Nhập giờ làm từ ${min} - ${max}`;
        }
    }
}
