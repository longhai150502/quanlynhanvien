function layThongTinTuForm(){
    var taiKhoan = document.getElementById("tknv").value;
    var hoTen = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var matKhau = document.getElementById("password").value;
    var ngayLam =document.getElementById("datepicker").value;
    var luongCoBan = document.getElementById("luongCB").value*1;
    var chucVu = document.getElementById("chucvu").value;
    var gioLam = document.getElementById("gioLam").value*1;
    var tongLuong;
    

    var nhanVien = new NhanVien(taiKhoan, hoTen, email, matKhau, ngayLam, luongCoBan, chucVu, gioLam, tongLuong);
    return nhanVien;
}


function renderDsNhanVien(nhanVienArr){
    var contentHTML = "";
    for(var index = 0; index < nhanVienArr.length; index++){
        var currentNhanVien = nhanVienArr[index];
        contentHTML += `
            <tr>
                <td>${currentNhanVien.taiKhoan}</td>
                <td>${currentNhanVien.hoTen}</td>
                <td>${currentNhanVien.email}</td>
                <td>${currentNhanVien.ngayLam}</td>
                <td>${currentNhanVien.chucVu}</td>
                <td>${currentNhanVien.tongLuong}</td>
                <td>${currentNhanVien.gioLam}</td>
                <td>${currentNhanVien.loaiNhanVien}</td>
                <td>
                    <button onclick="xoaNhanVien('${currentNhanVien.taiKhoan}')" class="btn btn-danger">Xóa</button>
                    <button onclick="suaNhanVien('${currentNhanVien.taiKhoan}')" data-toggle="modal" data-target="#myModal" class="btn btn-warning">Sửa</button>
                </td>
            </tr>
        `;
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function timKiemViTri(taiKhoanNhanVien, nhanVienArr){
    for (var index = 0; index<nhanVienArr.length; index++){
        var vitriNhanVien = nhanVienArr[index];
        if(vitriNhanVien.taiKhoan == taiKhoanNhanVien){
            return index;
        }
    }
    return -1;
}

function showThongTin(nhanVien){
    document.getElementById("tknv").value = nhanVien.taiKhoan;
    document.getElementById("name").value = nhanVien.hoTen;
    document.getElementById("email").value = nhanVien.email;
    document.getElementById("password").value = nhanVien.matKhau;
    document.getElementById("datepicker").value = nhanVien.ngayLam;
    document.getElementById("luongCB").value = nhanVien.luongCoBan;
    document.getElementById("chucvu").value = nhanVien.chucVu;
    document.getElementById("gioLam").value = nhanVien.gioLam;
}

function resetForm(){
    document.getElementById("formNhanVien").reset();
}